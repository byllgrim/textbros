textbros
================================================================================

A minimalistic and intuitive engine for text adventure games.

Target requirements:

  * Match features of ZORK, Hitchhiker's guide, etc
  * Text only! (Both playing and designing games).
  * Separate layers: Game engine, Core library, User creations


Misc TODOs (and sketches)
-------------------------

I'm thinking, every existing thing should simply be an OBJECT.
Playing the game is commandeering just one object.
(Think of some way to authenticate for multiplayer).

I also think that the game file representation shouldn't be unified.
Meaning that I want several equivalent ways of representing a game.
For instance, directories on a computer could maybe represent a world?
Or a json file, or tcl style dict.

Challenging language parsings:

  * (Easy thing: `verb object`)
  * Aliases:
    * "Go north" vs "north"
    * "Look at"
  * Combinations:
    * `verb object subject` (maybe not so difficult)
    * `... then ...`
    * `... with ...`
  * `take all` (how to do "all"?)
  * `clove of garlic` (several words make up just one object)
  * `climb down stairs`
  * `hello, troll`

What's in an object?:

  * Other objects
  * References to objects (name or id)
  * Properties (plain text)
  * `actions` that it can use?
  * `meta` information about it? (like "visited").
  * Allowable actions? Or word classes it can serve as?

Word classes:

  * (Is it necessary/useful to care about this?)
  * Noun, verb, adjective, adverb, pronoun, preposition, conjunction,
    interjection, numeral, article, determiner

English grammar:
```
(((S) (NP VP))
 ((VP) (V))
 ((VP) (V NP))
 ((V) died)
 ((V) employed)
 ((NP) nurses)
 ((NP) patients)
 ((NP) Medicenter)
 ((NP) "Dr Chan"))
 From: https://en.wikipedia.org/wiki/S-expression
```

> [Off-side rule] where increasing the indentation results in the lexer
> outputting an INDENT token, and decreasing the indentation results in the
> lexer outputting a DEDENT token.
https://en.wikipedia.org/wiki/Off-side_rule
