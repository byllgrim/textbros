Feature: Open Container

  Most games have containers that can hold items.
  Like "opening the mailbox reveals a leaflet".

  Scenario: You find the treasure
    Given you enter the captains quarters
    And there is a large embellished chest by the wall
    When you open the chest
    Then you should see bountiful loot
