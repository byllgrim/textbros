Feature: Take Item

  When a player sees something in the game,
  picking it up allows for it to be used later.

  Scenario: Finding a new pet rock
    Given a rock lays on the path before you
    When you take the rock
    Then the rock is taken

  Scenario: Tallying your inventory
    Given you posess a pet rock
    When you check your inventory
    Then you see your pet rock
