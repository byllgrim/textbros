Feature: Using Items

  You have an item and want to do something with it.

  Scenario: Lighting a lantern
    Given you hold a lantern
    When you light the lantern
    Then light shines from it

  Scenario: Smearing mayo on the bus driver
    Given you have mayo
    And you are on the bus
    When you use mayo on the bus driver
    Then the bus accellerates
