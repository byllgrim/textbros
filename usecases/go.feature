Feature: Go Somewhere

  A player should be able to move their character around.

  Scenario: Going down a trap door
    Given an open trap door is in front of you
    When you go down
    Then you end up in a dark and damp cellar
