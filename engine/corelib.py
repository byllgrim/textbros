"""Core library with handy functions for creating games"""

import io
import sys
import unittest

import tree


class Tests(unittest.TestCase):
    """Unit tests for core library functions"""

    def test_looking_in_empty_room(self):
        """Test if a player can see the room they are in"""

        world = tree.mknode_named("world")
        room = tree.mknode_named("room")
        player = tree.mknode_named("player")
        tree.add_node_to(room, world)
        tree.add_node_to(player, room)

        seized_output = io.StringIO()
        sys.stdout = seized_output
        look(world, player, [])
        sys.stdout = sys.__stdout__

        self.assertEqual("You are in the room\n", seized_output.getvalue())

    def test_go_north(self):
        """Test if relocating northwards work"""

        world = {"world": {}}
        hallway = {"hallway": {}}
        room = {"room": {"north": {"/world/hallway": {}}}}
        player = {"player": {"actions": {"go": {}}}}
        tree.add_node_to(player, room)
        tree.add_node_to(room, world)
        tree.add_node_to(hallway, world)

        go(world, player, ["north"])

        parent = tree.getparent_of_in(player, world)
        parentname = tree.getname_of(parent)
        self.assertEqual(parentname, "hallway")


def look(world, agent, args):
    """Let a player 'look' at their surroundings"""

    parent = tree.getparent_of_in(agent, world)
    print("You are in the", tree.getname_of(parent))


def go(world, agent, args):  # pylint: disable=invalid-name
    """Let a player move to another location"""

    direction = args[0]
    parent = tree.getparent_of_in(agent, world)
    dirnode = tree.getnode_named_in(direction, parent)
    dirbody = tree.getbody_of(dirnode)
    path = tree.getname_of(dirbody)

    destination = tree.getnode_at_in(path, world)

    tree.delete_node_in(agent, world)
    tree.add_node_to(agent, destination)


ACTIONS = {}
ACTIONS['look'] = look
ACTIONS['go'] = go


if __name__ == '__main__':
    unittest.main()
