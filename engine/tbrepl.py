#!/usr/bin/env python3

"""A REPL frontend, for separating engine logic into several small programs"""


import sys


def main():
    if len(sys.argv) != 3:
        print('usage:', sys.argv[0], '<gamefile> <charactername>')
        sys.exit()

    gamefile = sys.argv[1]
    charactername = sys.argv[2]

    while True:
        i = input()
        if len(i) > 0:
            print("TODO exec: tbcli", gamefile, charactername, i)


if __name__ == "__main__":
    main()
