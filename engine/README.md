# Engine

This directory contains source files for the actual game engine.


## Concepts

The game world is a "tree graph".

Happenings in the game consists of 1) manipulating the tree, and 2) presenting
information to players.


## Data structures

A node is a single-item dict: `{"nodename": nodebody}`

Its body is always a dict: `{"nodename": {}}`

The game world is a node containing nodes:
```
{
  "world": {
    "room1": {
      "player": {}
    },
    "room2": {
    }
  }
}
```
