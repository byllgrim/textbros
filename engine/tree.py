"""Library for manipulating TREE data structures (using dict)"""

import unittest


class Tests(unittest.TestCase):
    """Test suite for the functions related to tree manipulation"""

    def test_making_a_new_tree(self):
        """Test if the root node of a new tree can be created"""

        rootnode = mknode_named("root")

        self.assertEqual(rootnode, {"root": {}})

    def test_adding_more_nodes(self):
        """Test if several nodes can be added to a tree"""

        rootnode = mknode_named("root")
        add_node_to(mknode_named("child1"), rootnode)
        add_node_to(mknode_named("child2"), rootnode)

        self.assertEqual(rootnode, {"root": {"child1": {}, "child2": {}}})

    def test_deleting_nodes(self):
        """Deleting arbitrary node from a tree"""

        top = mknode_named("top")
        mid = mknode_named("mid")
        bot = mknode_named("bot")
        add_node_to(mid, top)
        add_node_to(bot, mid)

        delete_node_in(bot, top)
        self.assertEqual(top, {"top": {"mid": {}}})

    def test_finding_immediate_parent(self):
        """Test if a search can find the parent directly above a node"""

        parent = mknode_named("parent")
        child = mknode_named("child")
        add_node_to(child, parent)

        found = getparent_of_in(child, parent)
        self.assertEqual(parent, found)

    def test_finding_nested_parent(self):
        """Test if searching can find parent of a node deep in a tree"""

        top = mknode_named("top")
        left = mknode_named("left")
        right = mknode_named("right")
        child = mknode_named("child")
        add_node_to(left, top)
        add_node_to(right, top)
        add_node_to(child, left)

        found = getparent_of_in(child, top)
        self.assertEqual(left, found)

    def test_find_node_by_name(self):
        """Try to locate a node in a treebased on its name"""

        world = {"world": {"a": {}, "b": {"b1": {"b1x": {}}}}}

        found = getnode_named_in("b1", world)
        expected = {"b1": {"b1x": {}}}

        self.assertEqual(found, expected)

    def test_relocate_a_node(self):
        """Test moving a node from one parent to another"""

        top = {"top": {"a": {}, "b": {"node": {}}}}
        node = getnode_named_in("node", top)
        target = getnode_named_in("a", top)

        delete_node_in(node, top)
        add_node_to(node, target)

        expected = {"top": {"a": {"node": {}}, "b": {}}}
        self.assertEqual(top, expected)

    def test_searching_by_path(self):
        """Get a node by its absolute path"""

        top = {"top": {"mid": {"bot": {}}}}

        found = getnode_at_in("/top/mid", top)
        expected = {"mid": {"bot": {}}}

        self.assertEqual(found, expected)


def mknode_named(name):
    """Create a new node with the given name"""

    return {name: {}}


def add_node_to(node, target):
    """Set one node as a child of another node"""

    targetbody = getbody_of(target)
    targetbody[getname_of(node)] = getbody_of(node)


def delete_node_in(node, root):
    """Remove a node from a tree"""

    parentnode = getparent_of_in(node, root)
    parentbody = getbody_of(parentnode)
    nodename = getname_of(node)

    del parentbody[nodename]


def getname_of(node):
    """Get the name of a node"""

    keylist = list(node.keys())
    return keylist[0]  # A node is {name: body}


def getbody_of(node):
    """Get the body of a node"""

    valuelist = list(node.values())
    return valuelist[0]  # A node is {name: body}


def getparent_of_in(node, root):
    """Search a tree for parent of given node"""

    rootchilds = getbody_of(root)
    nodename = getname_of(node)

    if nodename in rootchilds.keys():
        return root

    for k in rootchilds.keys():
        parent = getparent_of_in(node, {k: rootchilds[k]})
        if parent is not None:
            return parent

    return None


def getnode_named_in(name, root):
    """Search tree for node with certain name"""

    rootname = getname_of(root)
    if rootname == name:
        return root

    rootbody = getbody_of(root)
    for key in rootbody.keys():
        newroot = {key: rootbody[key]}
        node = getnode_named_in(name, newroot)
        if node is not None:
            return node

    return None


def getnode_at_in(path, root):
    """Follow a path and return the end node"""

    splitted = path.split(sep="/")

    if len(splitted) < 2:
        return None

    rootname = getname_of(root)
    if splitted[1] != rootname:
        return None
    if (len(splitted) == 2) and (splitted[1] == rootname):
        return root

    nextname = splitted[2]
    rootbody = getbody_of(root)
    if not nextname in rootbody.keys():
        return None

    splitted.pop(1)
    newpath = "/".join(splitted)
    nextnode = getnode_named_in(nextname, root)
    return getnode_at_in(newpath, nextnode)


if __name__ == '__main__':
    unittest.main()
