# Save to JSON, then recover.

import json
import os


# Create a world and save it

world = {"world": {"room": {"player": {}}, "hallway": {}}}

print(world)
print(type(world))

with open('tmp.json', 'w') as f:
    json.dump(world, f)


# Load the saved file and inspect it

with open('tmp.json') as f:
    restored = json.load(f)

print(restored)
print(type(restored))


# Cleanup

os.remove('tmp.json')
