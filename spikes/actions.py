# How can an object perform actions?


def speak():
    print('speaking blabla')


def shine():
    print('shines bright like a sun')


player = {}
player['actions'] = {}
player['actions']['speak'] = 'speak'
player['actions']['sit'] = 'sit'  # Purposely unimplemented

actions = {}
actions['speak'] = speak
actions['shine'] = shine  # Pusposely extraneous

while True:
    i = input('tb: ')

    if (i in player['actions']) and (i in actions):
        actions[i]()
    else:
        print('?')
