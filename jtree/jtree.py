#!/usr/bin/env python3

import json
import sys


def read_from_stdin():
    lines = []
    for line in sys.stdin:
        lines.append(line)
    filecontent = "".join(lines)

    try:
        tree = json.loads(filecontent)
    except json.decoder.JSONDecodeError:
        print("error: bad json file", file=sys.stderr)
        sys.exit()

    return tree


def dump_to_stdout(tree):
    treestr = json.dumps(tree, indent=2)
    print(treestr)


def get_path_root(path):
    splitted = path.split("/")

    if len(splitted) >= 2:
        return splitted[1]
    else:
        return None


def get_node_name(node):
    keys = node.keys()
    keylist = list(keys)

    if len(keylist) >= 1:
        return keylist[0]
    else:
        return None


def get_path_tail(path):
    splitted = path.split("/")
    if len(splitted) <= 2:
        return ""

    splitted.pop(1)
    joined = "/".join(splitted)
    return joined


def get_node_childnames(node):
    if node is None:
        return None

    body = get_node_body(node)
    keys = body.keys()
    keylist = list(keys)
    return keylist


def get_node_body(node):
    values = node.values()
    valuelist = list(values)

    if len(valuelist) < 1:
        return None
    body = valuelist[0]
    return body


def create_node_child(name, node):
        body = get_node_body(node)
        body[name] = {}


def mknode(path, tree):
    pname = get_path_root(path)
    nname = get_node_name(tree)
    if pname != nname:
        return

    tail = get_path_tail(path)
    tname = get_path_root(tail)
    cnames = get_node_childnames(tree)
    if len(tail) <= 1:
        return
    if not tname in cnames:
        create_node_child(tname, tree)

    body = get_node_body(tree)
    if not tname in body:  # TODO why didn't I use this before?
        return  # TODO do something better?
    newroot = {tname: body[tname]}
    mknode(tail, newroot)


def get_path_end(path):
    splitted = path.split("/")

    if len(splitted[-1]) > 0:
        last = splitted[-1]
    else:
        last = splitted[-2]

    return last


def rm(path, tree):
    ename = get_path_end(path)
    body = get_node_body(tree)
    if ename in body:
        del body[ename]
        return

    if len(body) < 1:
        return
    tail = get_path_tail(path)
    rm(tail, body)


def run_cmd_on_tree(cmd, tree):
    if len(cmd) < 1:
        return
    cmdname = cmd[0]
    cmdargs = cmd
    cmdargs.pop(0)

    if cmdname == "mknode":
        if len(cmdargs) >= 1:  # TODO check _before_ cmdname case statement?
            path = cmdargs[0]
            mknode(path, tree)
    if cmdname == "rm":
        if len(cmdargs) >= 1:
            path = cmdargs[0]
            rm(path, tree)


if __name__ == "__main__":
    tree = read_from_stdin()

    cmd = sys.argv
    cmd.pop(0)
    run_cmd_on_tree(cmd, tree)

    dump_to_stdout(tree)
